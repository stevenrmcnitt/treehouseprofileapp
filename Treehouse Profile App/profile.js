/** 
	Author:
		Steven Reese McNitt, February 11th 2018
	Problem: 
		We need a simple way to look at a user's badge count and JavaScript points.
	Solution: 
		Use Node.js to connect to Treehouse's API to get profile information to print out.
	Notes:
		Parsing:
			The process of converting a string to a data structure is called 'parsing'.
		forEach():
			The forEach() method iterates over an array and 
			passes each member of the array into a callback function.
			Examples:
				I.  const fruits = ["apple", "banana", "pear", "watermelon"]
					fruits.forEach((fruit) => {
						console.log(fruit)
					})
				II. const names = ["Bob", "Steve", "Janet, "Tim"]	
					names.forEach(console.log(name))	
**/
// Include modules.
const https = require('https')
const http = require('http')

function printMessage(username, badgeCount, points) {
	const message = `${username} has ${badgeCount} badge(s) and ${points} JavaScript points.`
	console.log(message)
}

function printError(error) {
	console.log(error)
}

function get(username) {
	const URL = `https://teamtreehouse.com/${username}.json`

	try {
		// Connect to the API URL (https://teamtreehouse.com/<username>.json).
		const request = https.get(URL, (response) => {
							const statusCode = response.statusCode
							const responseError = `Failed response from ${URL}: ${statusCode} ${http.STATUS_CODES[statusCode]}.`
							const connectionEstablished = (statusCode >= 200 && statusCode < 300) ? true : false
							let body = ""

							if(!connectionEstablished) {
								printError(responseError)
								return
							}

							//Read the JSON data.
							response.on('data', (data) => {
								body += data.toString() // Converting buffer to string.
							})

							// Parse the JSON data. See note on Parsing.
							response.on('end', () => {
								const profile = JSON.parse(body)
								// Print the data.
								printMessage(username, profile.badges.length, profile.points.JavaScript)
							})
						})

		request.on('error', (error) => { 
			printError(`There was a problem with the request: ${error.message}`)
		})
	} catch(error) {
		printError(`An error occured: ${error.message}`)
	}
}

// Allow get() function to be called from other files
// which require() profile.js.
module.exports.get = get