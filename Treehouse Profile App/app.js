const profile = require('./profile')

// Receive user input from the command line
// as arguments for the apps main getProfile() method.
const users = process.argv.slice(2)
// See note on forEach().
users.forEach((username) => { profile.get(username) })